import os
import time
import re
from flask import request
import requests
import xml.etree.ElementTree as ET
from flask import Flask, render_template, jsonify, json


application = Flask(__name__)
app = application

@app.route("/")
def pool_info_website():
    return render_template('index.html')


@app.route('/pools', methods=['GET'])
def print_pools():
    all_pools =[]
    for i in range(len(pool_name1)):
        pool = {}
        pool['Name'] = pool_name1[i]
        pool['Type'] = pool_type1[i]
        pool['Status'] = status1[i]
        pool['Open_date'] = open_date1[i]
        pool['Weekday'] = weekday1[i]
        pool['Weekend'] = weekend1[i]
        all_pools.append(pool)

    return json.dumps(all_pools)


src = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"
data = requests.get(src)
root = ET.fromstring(data.text)
pool_name1, status1, phone1, pool_type1, open_date1, weekday1, weekend1 = [], [], [], [], [], [], []
pool_dict = {}

for pool in root.findall('row'):
    pool_name = ''
    pool_type = ''
    status = ''
    phone = ''
    open_date = ''
    weekday = ''
    weekend = ''

    try:
        pool_name = pool.find('pool_name').text
        pool_type = pool.find('pool_type').text
        status = pool.find('status').text
        phone = pool.find('phone').text
        open_date = pool.find('open_date').text
        weekday = pool.find('weekday').text
        weekend = pool.find('weekend').text
        pool_name1.append(pool_name), pool_type1.append(pool_type), status1.append(status), phone1.append(phone), open_date1.append(open_date), weekday1.append(weekday), weekend1.append(weekend)
    except AttributeError:
        continue


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')